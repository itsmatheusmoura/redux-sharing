import "./index.css";

function App() {
  const contador = 10;
  return (
    <div className="h-screen bg-neutral-800 flex flex-col p-4 items-center  justify-center">
      <div className="bg-neutral-700 w-2/5 p-6 rounded">
        <h1 className="text-2xl font-bold text-center text-neutral-300">
          Sharing contador
        </h1>
        <div className="flex flex-col items-center justify-center my-2">
          <h1 className="text-neutral-300">Contador</h1>
          <h1 className="text-3xl font-bold text-neutral-200">{contador}</h1>
        </div>
        <div className="flex flex-col">
          <input
            placeholder="Adicione um valor"
            className="w-full rounded p-2 my-2 bg-neutral-600 outline-none text-neutral-300"
            type="number"
          />
          <button className="bg-neutral-600 my-2 rounded py-2 text-neutral-300 font-bold  hover:opacity-75 transition-opacity duration-500 ease-in-out">
            Incrementar
          </button>
          <button className="bg-neutral-600 my-2 rounded py-2 text-neutral-300 font-bold  hover:opacity-75 transition-opacity duration-500 ease-in-out">
            Decrementar
          </button>
          <button
            disabled
            className="bg-neutral-600 my-2 rounded py-2 text-neutral-300 font-bold  hover:opacity-75 transition-opacity duration-500 ease-in-out"
            onClick={() => console.log("Teste")}
          >
            Somar valor input
          </button>
        </div>
      </div>
    </div>
  );
}

export default App;
